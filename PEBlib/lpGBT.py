import os
import logging
import subprocess
from lpgbt_control_lib import LpgbtV1
import sys
sys.path.append(os.path.abspath ('.')+"/UPL/software/lib")
import upl as usb_dongle

print("!!! Please source felx environment before run this script !!!")

# # low level access to control pins
# GPIO_MAP = [
#     LpgbtV0.CTRL_RSTB: 0,
#     LpgbtV0.CTRL_ADDR: 1,
#     # ... add a mapping to your hardware GPIOs for all control pins
# ]

def write_lpgbt_ctrl_pin(pin_name, value):
    pass
#     gpio_id = GPIO_MAP[pin_name]
#     # TODO: write 'value' to your hardware GPIO

def read_lpgbt_ctrl_pin(pin_name):
    return 1
#     gpio_id = GPIO_MAP[pin_name]
#     gpio_val = # TODO read value of hardware GPIO
#     return gpio_val

class lpGBT:
    def __init__(self, lpgbt_addr=0x70, commi="IC", USBnum=0):
        self.lpgbt_addr = lpgbt_addr & 0XFF
        self.commi = commi
        self.USBnum = USBnum & 0XFF

        if self.commi == "UPL":
            upl_list = usb_dongle.upl_scan().upl_list
            if USBnum in (upl_list):
                self.upl = usb_dongle.upl(dev = USBnum)
                print(">>> Opened UPL")
            else:
                print(">>> Please connect UPL!")
                return            
   
        # get a logger for lpGBT library logging
        self.lpgbt_logger = logging.getLogger("lpgbt")

        # instantiate lpGBT class
        self.lpgbt = LpgbtV1(logger=self.lpgbt_logger)        

        # register communications interface(s)
        if commi == "I2C":
            self.lpgbt.register_comm_intf(
                name="I2C",
                write_regs = self.flx_i2c_write_lpGBT,
                read_regs = self.flx_i2c_read_lpGBT,
                default=True
            )
        elif commi == "IC":
            self.lpgbt.register_comm_intf(
                name="IC",
                write_regs = self.flx_ic_write_lpGBT,
                read_regs = self.flx_ic_read_lpGBT,
                default=True
            )
        else:
            self.lpgbt.register_comm_intf(
                name="UPL",
                write_regs = self.upl_write_lpGBT,
                read_regs = self.upl_read_lpGBT,
                default=True
            )

        # register access methods to control pins
        self.lpgbt.register_ctrl_pin_access(
            write_pin=write_lpgbt_ctrl_pin,
            read_pin=read_lpgbt_ctrl_pin
        )

        # # communicate with your chip
        # lpgbt.generate_reset_pulse()
        # lpgbt.clock_generator_setup()
        # # etc

        # in order to use a different communication interface
        self.lpgbt.adc_convert(..., comm_intf="I2C")
        self.lpgbt.adc_convert(..., comm_intf="EC")
        self.lpgbt.adc_convert(..., comm_intf="IC")
        self.lpgbt.adc_convert(..., comm_intf="UPL")


        # low level access to I2C interface. Only for reading from or writing to timing lpGBT
        def flx_i2c_read_lpGBT(self, flx_link=0, vtrx_link=0, reg_addr=-1):
            if reg_addr == -1:
                subprocess.run('fscagbtxconf -G %d -C 0x%x -I 0x%x '%(flx_link,vtrx_link,lpgbt_addr))
            else:
                subprocess.run('fscagbtxconf -G %d -C 0x%x -I 0x%x -a 0x%x'%(flx_link,vtrx_link,lpgbt_addr,reg_addr))

        def flx_i2c_write_lpGBT(self, flx_link, vtrx_link, reg_addr, reg_val, file_name):
            flx_link = 0
            vtrx_link = 0            
            reg_addr &= 0X1FF
            reg_val &= 0X1FF
            file_name = "none"
            if file_name == 'none':
                subprocess.run('fscagbtxconf -G %d -C 0x%x -I 0x%x -a 0x%x 0x%x'%(flx_link,vtrx_link,lpgbt_addr,reg_addr,reg_val))
            else:
                subprocess.run('fscagbtxconf -G %d -C 0x%x -I 0x%x %s'%(flx_link,vtrx_link,lpgbt_addr,file_name))

        # low level access to IC interface. 
        def flx_ic_read_lpGBT(self, flx_link=0, reg_addr=-1):
            if lpgbt_addr == 0x70:
                if reg_addr == -1:
                    subprocess.run('fice -G %d -I 0x%x '%(flx_link,lpgbt_addr))
                else:
                    subprocess.run('fice -G %d -I 0x%x -a 0x%x'%(flx_link,lpgbt_addr,reg_addr))
            else:
                if reg_addr == -1:
                    subprocess.run('fice -G %d -I 0x%x -e'%(flx_link,lpgbt_addr))
                else:
                    subprocess.run('fice -G %d -I 0x%x -e -a 0x%x'%(flx_link,lpgbt_addr,reg_addr))


        def flx_ic_write_lpGBT(self, flx_link, reg_addr, reg_val, file_name):
            flx_link = 0             
            reg_addr &= 0X1FF
            reg_val &= 0X1FF
            file_name = "none"
            if lpgbt_addr == 0x70:
                if file_name == 'none':
                    subprocess.run('fice -G %d -I 0x%x -a 0x%x 0x%x'%(flx_link,lpgbt_addr,reg_addr,reg_val))
                else:
                    subprocess.run('fice -G %d -I 0x%x %s'%(flx_link,lpgbt_addr,file_name))
            else:
                if file_name == 'none':
                    subprocess.run('fice -G %d -I 0x%x -e -a 0x%x 0x%x'%(flx_link,lpgbt_addr,reg_addr,reg_val))
                else:
                    subprocess.run('fice -G %d -I 0x%x -e %s'%(flx_link,lpgbt_addr,file_name))

        # low level access to I2C interface for UPL. 
        def upl_read_lpGBT(self, reg_addr, read_len):
            return self.upl.read_regs(
                device_addr=self.addr,
                addr_width=2,
                reg_addr=(reg_addr & 0x1FF),
                read_len=read_len
            )                    

        def upl_write_lpGBT(self, reg_addr, reg_vals):
            reg_vals = [reg_val & 0xFF for reg_val in reg_vals]
            self.upl.write_regs(
                device_addr=self.addr,
                addr_width=2,
                reg_addr=(reg_addr & 0x1FF),
                data=bytes(reg_vals)
            ) 


print('lpGBT.py successfull !')