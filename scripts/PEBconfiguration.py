import subprocess
import time
import os
import sys
import traceback
sys.path.append("../PEBlib")
sys.path.append("../PEBlib/UPL/software/lib")
import lpGBTinit
import upl

# Config parameter
flx_link = 0
lpgbt_addr = 0x70
use_lumi = True

# lpGBT pre_emphasis config
lpgbt_modulation_current = 115
lpgbt_emphasis_amp = 50

# VTRx+ Pre_emphasis config
vtrx_bias_current = 80
vtrx_modulation_current = 85
use_vtrx_emphasis = True
vtrx_emphasis_amp = 3

def main():

    subprocess.run('flx')
    subprocess.run('flx-init')

    print("***************** Debug Info. *************************")
    # Configure the line driver when the link is unreliable
    ldconfig_h = (lpgbt_modulation_current & 0x7F) | 0x80
    ldconfig_l = lpgbt_emphasis_amp & 0x7F
    for x in range(2):
        subprocess.run('fice -1 -G %d -I 0x%x -a 0x3A 0x%x'%(flx_link,lpgbt_addr,ldconfig_l))
        time.sleep(1)
        subprocess.run('fice -1 -G %d -I 0x%x -a 0x3A 0x%x'%(flx_link,lpgbt_addr,ldconfig_h))
        time.sleep(1)
    print("*******************************************************")
    print('')

    # instantiate lpGBT class
    peb = lpGBTinit.lpGBTinit(lpgbt_addr=0x70, commi="IC", USBnum=0)

    peb.lpGBT_init(modulation_current = lpgbt_modulation_current, emphasis_amp = lpgbt_emphasis_amp)

    print("Timing lpGBT online.")

    if use_vtrx_emphasis:
        peb.vtrxp_timing_line_driver_setup(
            bias_current = vtrx_bias_current,
            modulation_current = vtrx_modulation_current,
            emphasis_amp = vtrx_emphasis_amp,
            emphasis_rising_edge_enable = True,
            emphasis_falling_edge_enable = True
        )

    peb.lpGBT_timing.lpgbt.adc_config(inp=1, inn=15, gain=0)
    print("Read RSSI from VTRx+: %d"%peb.lpGBT_timing.lpgbt.adc_convert(1))

    if(use_lumi):
        peb.lumi_add()
        peb.lumi_init(modulation_current = lpgbt_modulation_current, emphasis_amp = lpgbt_emphasis_amp)
        print("Lumi. lpGBT online.")

        if use_vtrx_emphasis:
            peb.vtrxp_lumi_line_driver_setup(
                bias_current = vtrx_bias_current,
                modulation_current = vtrx_modulation_current,
                emphasis_amp = vtrx_emphasis_amp,
                emphasis_rising_edge_enable = True,
                emphasis_falling_edge_enable = True
            )
        try:
            peb.vtrxp_lumi_enable()
        except Exception as e:
            print("Due to VTRx+ shares the i2c bus with modules (M0~M4), the power-off modules will block the i2c communication. We suggest to use other modules with Lumi.")
            raise e

    print('')
    time.sleep(1)
    cmd = "flx-info link"
    flx = os.popen(cmd)
    reply = flx.read()
    print(reply)
    if(use_lumi):
        if (-1==reply.find("Aligned | YES  YES")):
            print("Please re-launch init")
            return
    else:
        if (-1==reply.find("Aligned | YES")):
            print("Please re-launch init")
            return

    sync = sync_felix2lpgbt.Sync_Elink(file_FELIX)
    enable_timing, speed_timing = sync.module_on(0)
    enable_lumi, speed_lumi = sync.module_on(1)

    print("-----------Timing lpGBT setup-------------")
    peb.module_setup(module_id = 0, enable = enable_timing[0], reset = True, eprx_data_rate = speed_timing[0])
    peb.module_setup(module_id = 1, enable = enable_timing[1], reset = True, eprx_data_rate = speed_timing[0])
    peb.module_setup(module_id = 2, enable = enable_timing[2], reset = True, eprx_data_rate = speed_timing[0])
    peb.module_setup(module_id = 3, enable = enable_timing[3], reset = True, eprx_data_rate = speed_timing[0])
    peb.module_setup(module_id = 4, enable = enable_timing[4], reset = True, eprx_data_rate = speed_timing[1])
    peb.module_setup(module_id = 5, enable = enable_timing[5], reset = True, eprx_data_rate = speed_timing[1])
    peb.module_setup(module_id = 6, enable = enable_timing[6], reset = True, eprx_data_rate = speed_timing[1])
    peb.module_setup(module_id = 7, enable = enable_timing[7], reset = True, eprx_data_rate = speed_timing[1])
    peb.module_setup(module_id = 8, enable = enable_timing[8], reset = True, eprx_data_rate = speed_timing[2])
    peb.module_setup(module_id = 9, enable = enable_timing[9], reset = True, eprx_data_rate = speed_timing[2])
    peb.module_setup(module_id = 10, enable = enable_timing[10], reset = True, eprx_data_rate = speed_timing[2])
    peb.module_setup(module_id = 11, enable = enable_timing[11], reset = True, eprx_data_rate = speed_timing[2])
    peb.module_setup(module_id = 12, enable = enable_timing[12], reset = True, eprx_data_rate = speed_timing[3])
    peb.module_setup(module_id = 13, enable = enable_timing[13], reset = True, eprx_data_rate = speed_timing[3])

    if(use_lumi):
        print('')
        print("-----------Lumi. lpGBT setup-------------")
        peb.module_setup_lumi(module_id = 0, enable = enable_lumi[0])
        peb.module_setup_lumi(module_id = 1, enable = enable_lumi[1])
        peb.module_setup_lumi(module_id = 2, enable = enable_lumi[2])
        peb.module_setup_lumi(module_id = 3, enable = enable_lumi[3])
        peb.module_setup_lumi(module_id = 4, enable = enable_lumi[4])
        peb.module_setup_lumi(module_id = 5, enable = enable_lumi[5])
        peb.module_setup_lumi(module_id = 6, enable = enable_lumi[6])
        peb.module_setup_lumi(module_id = 7, enable = enable_lumi[7])
        peb.module_setup_lumi(module_id = 8, enable = enable_lumi[8])
        peb.module_setup_lumi(module_id = 9, enable = enable_lumi[9])
        peb.module_setup_lumi(module_id = 10, enable = enable_lumi[10])
        peb.module_setup_lumi(module_id = 11, enable = enable_lumi[11])
        peb.module_setup_lumi(module_id = 12, enable = enable_lumi[12])
        peb.module_setup_lumi(module_id = 13, enable = enable_lumi[13])


####################################################################################

if len(sys.argv) >= 2:
    file_FELIX = str(sys.argv[1])
else:
    file_FELIX = "./config/felix/default.yelc"
    print("Use default config file: default.yelc")

try:
    main()
except:
    traceback.print_exc()

